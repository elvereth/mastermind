import random

options = ['blue', 'green', 'red', 'yellow', 'white', 'black']
code = random.choices(options, k=4)
print(f'Computer\'s randomized code is: {code}')
tries = 10

def player_picks_colors():
    colors = []
    while len(colors) < 4:
        choice = input(f"Choose one color from {options} and type it (position {len(colors)+1})")
        if choice in options:
            colors.append(choice)
        else:
            print('mal!')
    print(f"You're code is: {colors}")
    return colors


for i in range(tries):

    guess = player_picks_colors()

    if guess == code:
        print('You win *dancing parrot*')
        break

    else:
        for color in guess:
            print (color)
            if color in code:
                for index in range(len(guess)):
                    print ('is ' +str(color) +' in position ' + str(index))
                    print('this is the guess index ' + str(color))
                    print('this is the code index ' + str(code[index]))
                    if color == code[index]:
                        print('YES')
                    else:
                        print('NO')
            else:
                print("-")
        tries -= 1
        print(f"You have {tries} tries left")