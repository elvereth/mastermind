

DESCRIPTION -----------------------------------------------------------------------

This is a regular mastermind game. The computer will choose a randomized 4-color-code from 6 differents colors available and will ask a person to try to guess the code.




FEATURES ---------------------------------------------------------------------

The person will have up to 10 tries.

If one of the colors is right and in the right position, the computer will return a '1' (for each correct one, could return '1' '1' '1' as if they were 3 black pegs from the regular game).

If one of the colors is right but in the wrong position, the computer will return a '0' (white peg).

If all the colors are wrong, the computer will tell you to try again.

If the answer is the computer's code, it will print 'You win' and finish the game.



WORKFLOW/TASKS --------------------------------------------------------------

1. Declare the variables (colors) using a list
2. The computer will choose a 4-color-code from the colors of the list.
3. Ask the person to guess for the first time.
4. Person enters the input.
5. Loop to compare the input to the code:
    If the input equals the code, print('You win') and break.
    if there are repeated colors:
        check if position is the same or not. depending of output, return 1 or 0s.
    if the input doesnt have any color match with the code, ask the person to try again.
6. After the 10th try, stop the game and the person loses.